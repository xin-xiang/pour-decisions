import { BrowserRouter, Routes, Route } from "react-router-dom";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import MainPage from "./MainPage";
import CreateAccount from "./CreateAccount";
import AccountPage from "./AccountPage";
import CreateFlavorForm from "./CreateFlavor";
import FlavorList from "./FlavorList";
import LoginForm from "./LoginForm";
import Nav from "./Nav";
import Explore from "./Explore";
import CreateCocktailForm from "./CreateCocktailForm";
import ListAllSpirits from "./LiquorCabinet";
import CreateLiquor from "./AddBottle";
import CocktailDetail from "./CocktailDetail";
import AddIngredient from "./AddIngredient";
import UpdateLiquor from "./UpdateBottle";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  return (
    <div>
      <BrowserRouter basename={basename}>
        <AuthProvider baseUrl={process.env.REACT_APP_API_HOST}>
          <Nav />
          <div className="container">
            <Routes>
              <Route path="/" element={<MainPage />} />
              <Route path="/create" element={<CreateAccount />} />
              <Route path="/accounts/:username" element={<AccountPage />} />
              <Route path="/flavors" element={<CreateFlavorForm />} />
              <Route path="/flavors-list" element={<FlavorList />} />
              <Route path="/cabinet" element={<ListAllSpirits />} />
              <Route path="/addbottle" element={<CreateLiquor />} />
              <Route path="/cabinet/:id" element={<UpdateLiquor />} />
              <Route path="/signup" element={<CreateAccount />} />
              <Route path="/login" element={<LoginForm />} />
              <Route path="/explore" element={<Explore />} />
              <Route path="/cocktails" element={<CreateCocktailForm />} />
              <Route
                path="/cocktails/:id/ingredients"
                element={<AddIngredient />}
              />
              <Route
                path="/cocktails/:cocktailId"
                element={<CocktailDetail />}
              />
              <Route
                path="/cocktails/:id/ingredients"
                element={<AddIngredient />}
              />
            </Routes>
          </div>
        </AuthProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
