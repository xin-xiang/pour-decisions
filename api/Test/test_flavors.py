from fastapi.testclient import TestClient
from main import app
from queries.flavors import FlavorRepository


client = TestClient(app)


class EmptyFlavorRepository:
    def get_all(self):
        result = [{"id": 5, "name": "smoky"}]
        return result


def test_get_all_flavors():
    app.dependency_overrides[FlavorRepository] = EmptyFlavorRepository
    response = client.get("/flavors")

    expected = [{"id": 5, "name": "smoky"}]

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected
