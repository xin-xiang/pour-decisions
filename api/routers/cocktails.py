from fastapi import APIRouter, Depends, Response
from typing import List, Union, Optional
from queries.cocktails import (
    CocktailIn,
    CocktailRepository,
    CocktailOut,
    JoinedCocktailOut,
    Error,
)
from queries.accounts import AccountOut
from authenticator import authenticator

router = APIRouter()


@router.post("/cocktails", response_model=Union[CocktailOut, Error])
def create_cocktail(
    cocktail: CocktailIn,
    current_account: AccountOut = Depends(
        authenticator.try_get_current_account_data
    ),
    repo: CocktailRepository = Depends(),
):
    return repo.create(cocktail, current_account["id"])


@router.get("/cocktails", response_model=Union[List[JoinedCocktailOut], Error])
def get_all(repo: CocktailRepository = Depends()):
    return repo.get_all()


@router.put(
    "/cocktails/{cocktail_id}", response_model=Union[CocktailOut, Error]
)
def update_cocktail(
    cocktail_id: int,
    cocktail: CocktailIn,
    repo: CocktailRepository = Depends(),
) -> Union[Error, CocktailOut]:
    return repo.update(cocktail_id, cocktail)


@router.get(
    "/cocktails/{cocktail_id}", response_model=Optional[JoinedCocktailOut]
)
def get_cocktail(
    cocktail_id: int,
    response: Response,
    repo: CocktailRepository = Depends(),
) -> JoinedCocktailOut:
    cocktail = repo.get_one(cocktail_id)
    if cocktail is None:
        response.status_code = 404
    return cocktail


@router.delete("/cocktails/{cocktail_id}", response_model=bool)
def delete_cocktail(
    cocktail_id: int,
    repo: CocktailRepository = Depends(),
) -> bool:
    return repo.delete(cocktail_id)


@router.get(
    "/accounts/{mixologist_id}/cocktails", response_model=List[CocktailOut]
)
def get_user_cocktails(
    mixologist_id: int,
    response: Response,
    repo: CocktailRepository = Depends(),
) -> List[CocktailOut]:
    cocktails = repo.get_user_cocktails(mixologist_id)
    if cocktails is None:
        response.status_code = 404
    return cocktails
