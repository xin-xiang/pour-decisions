import { useState, useEffect, useCallback } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { Link, useParams } from "react-router-dom";

const AddIngredient = () => {
  const [name, setName] = useState("");
  const [isSpirit, setIsSpirit] = useState("");
  const [quantity, setQuantity] = useState("");
  const { token } = useToken();
  const { id } = useParams();
  const [ingredientList, setIngredients] = useState([]);
  const handleIngredientCreation = async (e) => {
    e.preventDefault();

    const ingredientData = {
      name: name,
      is_spirit: isSpirit === "Yes",
      quantity: quantity,
    };

    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/cocktails/${id}/ingredients`,

      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(ingredientData),
      }
    );

    if (!response.ok) {
      const errorData = await response.json();
      console.error("Error:", errorData.message);
    } else {
      console.log("Ingredient all up in here!");
      fetchIngredients();
      e.target.reset();
    }
  };
  const fetchIngredients = useCallback(async () => {
    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/cocktails/${id}/ingredients`
    );
    if (response.ok) {
      const data = await response.json();
      setIngredients(data);
    }
  }, [id]);

  useEffect(() => {
    fetchIngredients();
  }, [fetchIngredients]);
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add Ingredient</h1>
          <form onSubmit={(e) => handleIngredientCreation(e)}>
            <div className="form-floating mb-3">
              <input
                required
                type="text"
                name="name"
                className="form-control"
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
              <label className="form-label">Ingredient Name</label>
            </div>
            <div className="form-floating mb-3">
              <select
                required
                name="isSpirit"
                className="form-select"
                value={isSpirit}
                onChange={(e) => {
                  setIsSpirit(e.target.value);
                }}
              >
                <option value="">Select</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </select>
              <label className="form-label">Is this an alcohol?</label>
            </div>
            <div className="form-floating mb-3">
              <input
                required
                type="text"
                name="quantity"
                className="form-control"
                onChange={(e) => {
                  setQuantity(e.target.value);
                }}
              />
              <label className="form-label">Quantity</label>
            </div>
            <button className="btn btn-primary">Add Ingredient</button>
          </form>
        </div>
        <h1>Your Cocktail Ingredients</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Is it an Alcohol?</th>
              <th>Quantity</th>
            </tr>
          </thead>
          <tbody>
            {ingredientList.map((ingredient) => (
              <tr key={ingredient.id}>
                <td>{ingredient.name}</td>
                <td>{ingredient.is_spirit ? "yes" : "nope"}</td>
                <td>{ingredient.quantity}</td>
                <td></td>
              </tr>
            ))}
          </tbody>
        </table>
        <Link to={`/cocktails/${id}`}> See My Cocktail </Link>
      </div>
    </div>
  );
};

export default AddIngredient;
