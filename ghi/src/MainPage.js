import "./Main.css";

function MainPage() {
  return (
    <div className="home-content px-4 py-5 my-5 text-center">
      <h1 className="display-5">Pour Decisions</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4"></p>
        <div
          id="carouselExampleFade"
          className="home-carousel carousel slide carousel-fade"
        >
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img
                src="https://media.istockphoto.com/id/1323890695/photo/pink-grapefruit-and-rosemary-gin-cocktail-is-served-in-a-prepared-gin-glasses.jpg?s=612x612&w=0&k=20&c=WItooyAnP4jsuzmBrGNClWrJcIEA2TevABIpuG-ZhPw="
                className="d-block w-100"
                alt="..."
              />
            </div>
            <div className="carousel-item">
              <img
                src="https://media.istockphoto.com/id/502072256/photo/craft-cocktail-assortment-on-well-lit-bar.jpg?s=612x612&w=0&k=20&c=zmxOgUMZW1kB--D-g8RABi391Ma39zemxeUyjWFb7R0="
                className="d-block w-100"
                alt="..."
              />
            </div>
            <div className="carousel-item">
              <img
                src="https://www.cocktailmag.fr/media/k2/items/cache/cocktails-classiques.jpg"
                className="d-block w-100"
                alt="..."
              />
            </div>
          </div>
          <button
            className="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleFade"
            data-bs-slide="prev"
          >
            <span
              className="carousel-control-prev-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button
            className="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleFade"
            data-bs-slide="next"
          >
            <span
              className="carousel-control-next-icon"
              aria-hidden="true"
            ></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
