import React, { useState, useEffect, useCallback } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import ListAllCocktails from "./ListAllCocktails";

function AccountPage() {
  const [accounts, setAccounts] = useState([]);
  const { fetchWithToken, token } = useToken();
  const [id, setID] = useState("");

  const fetchAccounts = useCallback(
    async (username) => {
      try {
        const data = await fetchWithToken(
          `${process.env.REACT_APP_API_HOST}/api/accounts/${username}`
        );
        setAccounts([data]);
      } catch (error) {
        console.error("Error fetching user accounts:", error);
      }
    },
    [fetchWithToken]
  );

  useEffect(() => {
    if (token) {
      const decodedToken = JSON.parse(atob(token.split(".")[1]));
      const user = decodedToken.account.username;
      const id = decodedToken.account.id;
      setID(id);

      if (user !== accounts[0]?.username) {
        fetchAccounts(user);
      }
    }
  }, [fetchAccounts]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <h1>My Account</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Username</th>
            <th>Email</th>
            <th>Birthday</th>
          </tr>
        </thead>
        <tbody>
          {Array.isArray(accounts) &&
            accounts.map((account) => (
              <tr key={account.id}>
                <td>{account.username}</td>
                <td>{account.email}</td>
                <td>{account.birthday}</td>
              </tr>
            ))}
        </tbody>
      </table>
      {id && <ListAllCocktails id={id} />}
    </>
  );
}

export default AccountPage;
