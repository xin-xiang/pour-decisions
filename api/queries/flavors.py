from pydantic import BaseModel
import os
from typing import List, Union, Optional
from psycopg_pool import ConnectionPool


pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class Error(BaseModel):
    message: str


class FlavorIn(BaseModel):
    name: str


class FlavorOut(BaseModel):
    id: int
    name: str


class FlavorRepository:
    def delete(self, flavor_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM flavor_profile
                        WHERE id = %s;
                        """,
                        [flavor_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_one(self, flavor_id: int) -> Optional[FlavorOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name
                        FROM flavor_profile
                        WHERE id = %s
                        """,
                        [flavor_id],
                    )
                    record = result.fetchone()
                    return self.record_to_flavor_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that flavor"}

    def get_by_name(self, name: str) -> Union[Error, List[FlavorOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, name
                        FROM flavor_profile
                        WHERE name = %s;
                        """,
                        [name],
                    )
                    record = db.fetchone()
                    if record:
                        id, name = record
                        return FlavorOut(id=id, name=name)
                    else:
                        return None
        except Exception:
            return Error(message="Could not retrieve flavor by name")

    def get_all(self) -> Union[Error, List[FlavorOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name
                        FROM flavor_profile
                        ORDER BY id;
                        """
                    )
                    return [
                        self.record_to_flavor_out(record) for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not get all flavors"}

    def create(self, flavor: FlavorIn) -> FlavorOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO flavor_profile
                            (name)
                        VALUES
                            (%s)
                        RETURNING id;
                        """,
                        [flavor.name],
                    )
                    id = result.fetchone()[0]
                    return FlavorOut(id=id, name=flavor.name)
        except Exception:
            return Error(message="Could not create flavor")

    def record_to_flavor_out(self, record):
        return FlavorOut(
            id=record[0],
            name=record[1],
        )
