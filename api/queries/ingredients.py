import os
from psycopg_pool import ConnectionPool
from pydantic import BaseModel
from typing import List

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class IngredientIn(BaseModel):
    name: str
    is_spirit: bool
    quantity: str


class IngredientOut(BaseModel):
    id: int
    name: str
    is_spirit: bool
    quantity: str
    cocktail_id: int


class IngredientRepository(BaseModel):
    def create(
        self, cocktail_id: int, ingredient: IngredientIn
    ) -> IngredientOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                try:
                    db.execute(
                        """
                        INSERT INTO ingredients
                            (name, is_spirit, quantity, cocktail_id)
                            VALUES
                            (%s, %s, %s, %s)
                            RETURNING id;
                        """,
                        [
                            ingredient.name,
                            ingredient.is_spirit,
                            ingredient.quantity,
                            cocktail_id,
                        ],
                    )
                except Exception as e:
                    print(e)
                id = db.fetchone()[0]

                print(
                    id,
                    ingredient.name,
                    ingredient.is_spirit,
                    ingredient.quantity,
                    cocktail_id,
                )
                return IngredientOut(
                    id=id,
                    name=ingredient.name,
                    is_spirit=ingredient.is_spirit,
                    quantity=ingredient.quantity,
                    cocktail_id=cocktail_id,
                )

    def get_ingredients(self, cocktail_id: int) -> List[IngredientOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT id, name, is_spirit, quantity, cocktail_id
                    FROM ingredients
                    WHERE cocktail_id = %s
                    ORDER BY id;
                    """,
                    [cocktail_id],
                )
                ingredients = [
                    IngredientOut(
                        id=row[0],
                        cocktail_id=row[4],
                        name=row[1],
                        is_spirit=row[2],
                        quantity=row[3],
                    )
                    for row in db
                ]
                return ingredients

    def update_ingredient(
        self, cocktail_id: int, ingredient_id: int, ingredient: IngredientIn
    ) -> IngredientOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    UPDATE ingredients
                    SET name = %s, is_spirit = %s, quantity = %s
                    WHERE id = %s AND cocktail_id = %s
                    RETURNING *;
                    """,
                    [
                        ingredient.name,
                        ingredient.is_spirit,
                        ingredient.quantity,
                        ingredient_id,
                        cocktail_id,
                    ],
                )
                row = result.fetchone()
                if row:
                    return IngredientOut(
                        id=row[0],
                        cocktail_id=row[4],
                        name=row[1],
                        is_spirit=row[2],
                        quantity=row[3],
                    )
                return None

    def delete_ingredient(self, cocktail_id: int, ingredient_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    DELETE FROM ingredients
                    WHERE id = %s AND cocktail_id = %s;
                    """,
                    [ingredient_id, cocktail_id],
                )
                return result.rowcount > 0
