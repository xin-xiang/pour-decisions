import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const Explore = () => {
  const [cocktails, setCocktails] = useState([]);

  const getCocktails = async () => {
    const response = await fetch(`${process.env.REACT_APP_API_HOST}/cocktails`);

    if (response.ok) {
      const data = await response.json();
      setCocktails(data);
    }
  };

  useEffect(() => {
    getCocktails();
  }, []);

  return (
    <>
      <div className="row row-cols-1 row-cols-md-3 g-4">
        {cocktails.map((cocktail) => {
          return (
            <div className="col" key={cocktail.id}>
              <div className="card text-center mb-3">
                <img
                  src={cocktail.picture_url}
                  className="card-img-top"
                  alt="..."
                />
                <div className="card-body">
                  <h3 className="card-title">{cocktail.cocktail_name}</h3>
                  <p className="card-text">{cocktail.description}</p>
                </div>
                <div className="card-body">
                  <Link
                    to={`/cocktails/${cocktail.id}`}
                    className="card-link"
                  >
                    Details
                  </Link>
                  <h5>Created by {cocktail.username}</h5>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default Explore;
