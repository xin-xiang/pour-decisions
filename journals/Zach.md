### 10-26-2023

So for the past couple days me and Toran have been working on deployment and getting bugs fixed one by one. The one thing that was kind of killer for us and took a decent amount of testing was that all of the urls and paths that we wrote our backend in all pointed to localhost, and now they need to point to the API APP url or else our code won't work. So we had to have everyone comb through and make sure all fetch requests and post urls are all working properly.

Then we started to make thuroughly test out everything on the deployed website and handled one bug at a time. This was also sort of frustrating becuase we had to push to ain everytime and wait for the pipeline to pass, and reset the backend to be able to see if our fixes actually worked or not. But we got most of them figured out. We are still dealing with the 404 on every page, but there is a fix for that, Bart posted it and said it was something to do with the way the cli was set up.

We are so close, we just forgot to add in a update function, somehow it slipped through the cracks. We have all the backend written for this but we still need to get it as a front end component. Were working on tat now. Then we just have a couple small things to fix if we want to, they are not site breaking but more of a "nice to have" deal.

The one thing I am a bit dissappointed in is that we do not have much styling to present tomorrow, but the plain architecture of the project took way longer than we thought it would. But I learned a ton through this process so its good that it happened, and we all gained valuable knowledge through the process. 

### 10-24-2023

For the past two days me and Toran have been working on finishing up some of our front end points. He was waiting on me to finish up the detail page so he could add a link from every tile in the explore page that would bring them to the cocktail page itself. So once I got that done, we both knocked out thye explore page by just mapping through all the cocktails.

We actually had to change some of our backend code base to help make this work. We were getting a bunch of errors that .map wasnt a function or can't map values of none. We realized that on some of the cocktail there were field that were optional that map was having a problem with mapping. So we changed those field to not optional in the models/classes in the queries. We also changed our joins in the get_all cocktails to just be a join and not a full outer join becuase that too is going to be causeing the map function to look over fields of null, and thats causing the issue in our front end. Took us a few hours but we did get this ironed out.

Then Toran and I started on deployment today as well.

### 10-19-2023

Continued to work on front end components today as well. I switched up to a cocktail detail page today. This was cool to get into as I had to make two seperate API calls to get the info I wanted. I had to do this because for each cocktail we have some information stored in the cocktail table, but for all the ingredients we stored those in an ingredients table that I have to call for where the cocktail id matches in both tables.

There was an Ah ha moment in here for me today. I havent really used useParams too much since learning about React. So I got to use them for the first time today. The tricky part of this was I thought it was returning a string from the useParams hook, but I needed an integer. So I kept trying to make the string number an integer in different ways. But in reality the useParams hook was actually returning a dictionary, so all I had to do was go in and access it like params.user_id and it started to work.

Then I was also trying to create sub rows in one coloumn of a table in React and couldn't find any resources on how to do this beside with a drop down in the table. I didn't want the drop down so instead I just made a seperate table for just ingredients on the cocktail detail page.

### 10-18-2023

Over the past two day I have started to work on my front end components for cocktails. Today was a frustrating day for me because I spent all day trying to make sure that the user ID was being passed along with the other required information for creating a cocktail, becuase I wanted to make sure that the user info for whoever created the cocktail was going to be able to be accessed later on when we implement our explore page. This kept giving me an error that it could not create the cocktail. This ended up being becuase I kept trying to pass in the user id or username along with the other info when all I needed is what was on the form and it would grab the user id from the query and implement it on the cocktail out.

It was a HUGE Ah ha moment when that happened. Though I did feel like an idiot getting caught up for so long on an error that was my own causing. But once I understood the loggic, I felt better that it was working the way I planned.

Then Dana and I had to have a conversation about how we were going to handle adding ingredients to a cocktail as adding ingredients is a seperate form from creating a cocktail as all the ingredients are going to a seperate table and not the cocktails table. So Dana did end up implementing a navigate line on successful creation of a cocktail that will bring you to the add ingredients form for the same cocktail, as the cocktail does have to exist before you can add ingredients to it as well. Then as you add ingredients to the cocktail these will populate a table below the ingredients form.

### 10-16-2023

This was another long day of just coding my backend points. I got my routers and queries all set up.
I did have to set up a couple handling functions in my queries that would take a cocktail in and make sure it was the right model for what was expected on the way out. Also to guarantee we were going to get the right data out, I had to make a couple new pydantic classes to make sure we got the right data out that we would need on the front end. These classes would be the UpdatedCocktailIn and the JoinedCocktailOut. The functions I added in the queries are the cocktail_in_to_out and the record_to_cocktail_out. These were both used in all my queries except the delete query for cocktails, again just to make sure I got the right types and data points out.

Again, there was no real discussion of design aspects today. We were basically all still writing our endpoints in the backend. Then we were helping each other out with syntax or smaller bugs that may have come up through the GUI.

This was a grand Ah ha day as I was able to get most of my endpoints working through the GUI on the /docs page. It was pretty cool to be able to see the results of the past weeks worth of work and get the proper responses back, at least for now. I'm starting to get the feeling that the front end may be a bit more touchy when it comes to making sure all the data we need is there and the data is flowing through the app properly.

### 10-13-2023

Really started in on my backend the past two days. I was tasked with doing the cocktail endpoints which means I should have a CRUD for cocktails, though we will not be using the full CRUD in the front end, but we want to have a full crud to help us develop the site.

The first thing I tackled was making my classes for FastAPI to know what to expect when doing a CocktailIn and CocktailOut and making sure the pydantic types were set to what we wanted them as.

Then I started in my first query for cocktails which was the create cocktail query. As this was my first time writing SQL statements to insert data. This is when BeeKeeper really came in handy. I was able to see my querys go into the data base based on a succesful request. This was sort of an Ah ha moment because things began to click about how to insert data to a SQL table based on a query / route that I wrote.

We didn't really talk about desing the past couple of days as everyone has moved onto working on their own backend points. Kind of a stand still on planning as we all get some code into the code base.

### 10-11-2023

Today we put the finishing touches on our tables based on some discussion that we had in the breakout room today. We wanted to redo our cocktails table. Originaly we wanted to have spirits in the cocktails table and then have ingredients be a seperate table that you would put all the non alcoholic ingredients into and link them through the cocktail id. We made a change so that were going to put every ingredient including the alcohol into the ingredients table and then include a boolean value in the table to see if the ingredient is alcholic so we could possibly link that to the alcohols in the users cabinet.

Then instead of an Ah Ha moment today, I would say there was a lack of one. We were trying to code in data to our flavor profiles table without having to add them through the GUI. We were never able to get the sytntax or code right in the migration file for them to populate in beekeeper. After a while we decided to just move on and add them through the GUI once we get the endpoint set up. I think this will be a little annoying to have to populate that data over and over again as we switch branches or go through deployment. So if I ever have some extra time outside of group responsibilities I might take a crack at trying to fix that. Leaving it alone for now.

Tomorrow starts my real work on the backend.

### 10-10-2023

Today we were still mob programming as we moved into creating the rest of our tables. I think this will be the last large bit of mob programming as we have assigned endpoint to each person to star working on their own. This will be nice to start trying my hand independantly on some of the newer things we have learned.

We did run into some design choices that we had to make. one of these, is how we want to incorperate multiple liquors and multiple ingredients into one cocktail. Were going to have the table for cocktails have up to four columns for liquors but only make one column required and theycan add more if they want to. Then were going to join the ingredients table with the cocktails table to link the ingredients for a specific cocktail through the cocktail id. (At least thats what I am understanding right now)

Tomorrow through the end of the week, we should all be done, or close to done, with the endpoints. Tomorrow we will get our hands durty with some actual code outside of SQL and Auth. Looking forward to it to see what I am able to accomplish over the next couple days

### 10-9-2023

Today we were reviewing code that Dana had completed over break in regards to Auth and ironing out some of the issues. There was an issue with the way the account out was working with the hashed password that we had to then create. Dana walked us through that code.

We did have one ah ha moment where we were testing out Auth endpoints for logging in, out, and creating user. We kept thnkning that the 422 repsonse was an error on our end but a HMU post allowed us to realize this was not an actual problem, jsut a sample error.

Then we also figured out through some trial and error, that we are requiring users to log in with their password when we thought it was going to be their username. We left this the way it was so now we can all succesfully login using a sample email and sample password.

### 9-28-2023

Today we kept mob coding together as we are all feeling a little uneasy about FastApi and how things actually are working in it. The goal was to ge Auth set up. Which we eventually did.

The Ah ha moment came from when we were able to actually get some of our end points to show up in the browser and see the get and post requests in there.

I think we still have to get a few issues ironed out with the request for Auth. I am starting to get a little worried about where we are in regard to time left to get this projects back end done so we can move onto the front end. I feel like we are starting to fall behind the eight ball, but i also have to keep reminding myself that thi is the first time that any of us have worked with this framework and making our own data tables, so trying to stay
optimistic.

### 9-27-2023

We were mob coding today trying to work through how to get our first table set up with PostgreSQL. We eventually did get it and were able to see it in the terminal. All group members worked on this.

The conversation on design aspects for the time being was that we will most likely eed to getht hings set up with a UUID eventually, but for now, we were keeping user_id as a serial integer.

The one sort of Ah Ha moment we had today was when we did see our table rendered in the terminal. It gave us some confidence that we were doing things right.
