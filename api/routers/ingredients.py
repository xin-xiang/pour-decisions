from fastapi import APIRouter, Depends, HTTPException, Path, Body
from queries.ingredients import (
    IngredientIn,
    IngredientOut,
    IngredientRepository,
)
from typing import List

router = APIRouter()


@router.post(
    "/cocktails/{cocktail_id}/ingredients", response_model=IngredientOut
)
def create_ingredient(
    cocktail_id: int,
    ingredient: IngredientIn,
    repo: IngredientRepository = Depends(),
):
    return repo.create(cocktail_id, ingredient)


@router.get(
    "/cocktails/{cocktail_id}/ingredients", response_model=List[IngredientOut]
)
def get_ingredients(
    cocktail_id: int,
    repo: IngredientRepository = Depends(),
):
    return repo.get_ingredients(cocktail_id)


@router.put(
    "/cocktails/{cocktail_id}/ingredients/{ingredient_id}",
    response_model=IngredientOut,
)
def update_ingredient(
    cocktail_id: int = Path(..., description="ID of the cocktail"),
    ingredient_id: int = Path(..., description="ID of the ingredient"),
    ingredient: IngredientIn = Body(..., description="Update ingredient data"),
    repo: IngredientRepository = Depends(),
):
    updated_ingredient = repo.update_ingredient(
        cocktail_id, ingredient_id, ingredient
    )
    if updated_ingredient:
        return updated_ingredient
    else:
        raise HTTPException(status_code=404, detail="Ingredient aint there")


@router.delete(
    "/cocktails/{cocktail_id}/ingredients/{ingredient_id}",
    response_model=List[IngredientOut],
)
def delete_ingredients(
    cocktail_id: int,
    ingredient_id: int,
    repo: IngredientRepository = Depends(),
):
    deleted = repo.delete_ingredient(cocktail_id, ingredient_id)

    if not deleted:
        raise HTTPException(status_code=404, detail="Ingredient aint there")

    remaining_ingredients = repo.get_ingredients(cocktail_id)

    return remaining_ingredients
