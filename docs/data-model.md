# Data models

---

### User

| name            | type     | unique | optional |
| --------------- | -------- | ------ | -------- |
| username        | string   | no     | no       |
| hashed_password | string   | no     | no       |
| email           | string   | yes    | no       |
| birthday        | datetime | no     | no       |

The `User` entity contains the data about a specific information
about a user.

---

### Spirits

| name        | type                     | unique | optional |
| ----------- | ------------------------ | ------ | -------- |
| type        | string                   | false  | false    |
| brand       | string                   | false  | false    |
| picture_url | string                   | false  | true     |
| description | string                   | false  | true     |
| owner       | reference to user entity | false  | false    |

### Flavors

| name | type   | unique | optional |
| ---- | ------ | ------ | -------- |
| name | string | no     | no       |

### Cocktails

| name           | type                       | unique | optional |
| -------------- | -------------------------- | ------ | -------- |
| cocktail_name  | string                     | no     | no       |
| flavor_profile | reference to Flavor entity | no     | no       |
| picture_url    | string                     | no     | no       |
| description    | string                     | no     | no       |
| mixologist     | integer                    | no     | no       |

### Ingredients

| name        | type    | unique | optional |
| ----------- | ------- | ------ | -------- |
| name        | string  | no     | no       |
| is_spirit   | boolean | no     | no       |
| quantity    | string  | no     | no       |
| cocktail_id | integer | no     | no       |
