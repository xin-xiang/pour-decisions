import useToken from "@galvanize-inc/jwtdown-for-react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const LoginForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { login } = useToken();
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    login(username, password);
    e.target.reset();
    navigate("/");
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Login!</h1>
          <form onSubmit={(e) => handleSubmit(e)}>
            <div className="form-floating mb-3">
              <input
                required
                type="text"
                name="username"
                className="form-control"
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
              />
              <label className="form-label">Username</label>
            </div>
            <div className="form-floating mb-3">
              <input
                required
                type="password"
                name="password"
                className="form-control"
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
              />
              <label className="form-label">Password</label>
            </div>
            <button className="btn btn-primary">Login!</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default LoginForm;
