import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";

const UpdateLiquor = () => {
  const params = useParams();
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [type, setType] = useState("");
  const [description, setDescription] = useState("");
  const { token } = useToken();
  const handleLiquorUpdate = async (e) => {
    e.preventDefault();

    const liquorData = {
      brand: name,
      type: type,
      description: description,
      picture_url: "",
    };

    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/spirits/${params.id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(liquorData),
      }
    );

    if (!response.ok) {
      const errorData = await response.json();
      console.error("Error:", errorData.message);
    } else {
      e.target.reset();
      navigate("/cabinet");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Update Liquor</h1>
          <form onSubmit={(e) => handleLiquorUpdate(e)}>
            <div className="form-floating mb-3">
              <input
                required
                type="text"
                name="name"
                className="form-control"
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
              <label className="form-label">Bottle Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                required
                type="text"
                name="type"
                className="form-control"
                onChange={(e) => {
                  setType(e.target.value);
                }}
              />
              <label className="form-label">Type</label>
            </div>
            <div className="form-floating mb-3">
              <input
                required
                type="text"
                name="description"
                className="form-control"
                onChange={(e) => {
                  setDescription(e.target.value);
                }}
              />
              <label className="form-label">Description</label>
            </div>
            <button className="btn btn-primary">Update Bottle</button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default UpdateLiquor;
