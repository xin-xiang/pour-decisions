from queries.ingredients import IngredientIn, IngredientOut


class MockIngredientRepository:
    def __init__(self):
        self.ingredients = []

    def create(
        self, cocktail_id: int, ingredient: IngredientIn
    ) -> IngredientOut:
        new_ingredient = IngredientOut(
            id=len(self.ingredients) + 1,
            name=ingredient.name,
            is_spirit=ingredient.is_spirit,
            quantity=ingredient.quantity,
            cocktail_id=cocktail_id,
        )
        self.ingredients.append(new_ingredient)
        return new_ingredient

    def get_ingredients(self, cocktail_id: int):
        return [
            ingredient
            for ingredient in self.ingredients
            if ingredient.cocktail_id == cocktail_id
        ]


def test_create_ingredient():
    repo = MockIngredientRepository()
    ingredient_in = IngredientIn(
        name="Test Ingredient", is_spirit=False, quantity=1
    )
    ingredient_out = repo.create(cocktail_id=1, ingredient=ingredient_in)

    assert ingredient_out is not None
    assert ingredient_out.id == 1
    assert ingredient_out.name == "Test Ingredient"
    assert ingredient_out.is_spirit is False
    assert ingredient_out.quantity == "1"
    assert ingredient_out.cocktail_id == 1


def test_get_ingredients():
    repo = MockIngredientRepository()

    ingredient1 = IngredientIn(
        name="Ingredient 1", is_spirit=False, quantity=1
    )
    ingredient2 = IngredientIn(name="Ingredient 2", is_spirit=True, quantity=2)
    ingredient3 = IngredientIn(
        name="Ingredient 3", is_spirit=False, quantity=3
    )

    repo.create(cocktail_id=1, ingredient=ingredient1)
    repo.create(cocktail_id=2, ingredient=ingredient2)
    repo.create(cocktail_id=1, ingredient=ingredient3)

    ingredients_for_cocktail_1 = repo.get_ingredients(cocktail_id=1)

    assert len(ingredients_for_cocktail_1) == 2
    assert ingredients_for_cocktail_1[0].name == "Ingredient 1"
    assert ingredients_for_cocktail_1[1].name == "Ingredient 3"
