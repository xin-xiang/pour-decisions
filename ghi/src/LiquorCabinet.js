import React, { useState, useEffect, useCallback } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { Link } from "react-router-dom";

function ListAllSpirits() {
  const [spirits, setSpirits] = useState([]);
  const { fetchWithToken, token } = useToken();

  const fetchSpirits = useCallback(async () => {
    try {
      const data = await fetchWithToken(
        `${process.env.REACT_APP_API_HOST}/spirits`
      );

      setSpirits(data);
    } catch (error) {
      console.error("Error fetching spirits:", error);
    }
  }, [fetchWithToken]);

  const handleDeleteSpirit = async (spiritId) => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/spirits/${spiritId}`,
        {
          method: "DELETE",
        }
      );

      if (response.ok) {
        fetchSpirits();
      } else {
        const errorData = await response.json();
        console.error("Error deleting spirit:", response.status, errorData);
      }
    } catch (error) {
      console.error("Error deleting spirit:", error);
    }
  };

  useEffect(() => {
    if (token && spirits.length === 0) {
      fetchSpirits();
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <h1>Liquor Cabinet</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Type</th>
            <th>Brand</th>
            <th>Description</th>
            <th>Modify</th>
          </tr>
        </thead>
        <tbody>
          {spirits.map((spirit) => (
            <tr key={spirit.id}>
              <td>{spirit.type}</td>
              <td>{spirit.brand}</td>
              <td>{spirit.description}</td>
              <td>
                <Link to={`/cabinet/${spirit.id}`}>
                  <button>Change Bottle</button>
                </Link>
              </td>
              <td>
                <button onClick={() => handleDeleteSpirit(spirit.id)}>
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <Link to="/addbottle">
        <button className="btn btn-primary">Add A Bottle</button>
      </Link>
    </>
  );
}

export default ListAllSpirits;
