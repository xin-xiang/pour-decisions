# APIs

## Cocktails

- **Method**: `POST`, `GET`, `GET`, `PUT`, `DELETE`,
- **Path**: `/cocktails`, `/cocktails/{cocktail_id}`

Input:

```json
{
  "cocktail_name": string,
  "flavor_profile": int,
  "picture_url": string,
  "description": string
}
```

Output:

```json
{
  "id": int,
  "cocktail_name": string,
  "flavor_profile": int,
  "picture_url": string,
  "description": string,
  "mixologist": int
}
```

Creating a new cocktail saves the name, flavor, picture, description. This adds a new existing cocktail to the database which can be shown on exploration page by a user .

## Flavors

- **Method**: `POST`,`GET`, `GET`, `DELETE`,
- **Path**: `/flavors`, `/flavors/{flavor_id}`

Input:

```json
{
  "name": string
}
```

Output:

```json
{
  "name": string,
  "id": int
}
```

Creating a new flavor saves the name of the flavor. This adds a new existing Flavor to the database which can be used to label a cocktail recipe's flavor.

## Accounts

- Method: `GET`, `POST`
- Path: `/api/accounts`, `/token`

Input:

```json
{
  "username": string,
  "password": string,
  "email": string,
  "birthday": date
}
```

Output:

```json
{
  "id": string,
  "username": string,
  "hashed_password": string,
  "email": string,
  "birthday": date
}
```

The Accounts API will create a token and post an account for a user on the Pour Decisions website. Users will need to enter in all of the information listed to create an account. The birthday field will be used to determine whether the user is old enough to access the website.

## Ingredients

- Method: `GET`, `POST`, `PUT`, `DELETE`
- Path: `/cocktails/{cocktail_id}/ingredients"`, `/cocktails/{cocktail_id}/ingredients/{ingredient_id}`

Input:

```json
{
  "name": string,
  "is_spirit": boolean,
  "quantity": string
}
```

Output:

```json
{
  "id": int,
  "name": string,
  "is_spirit": boolean,
  "quantity": string,
  "cocktail_id": int
}
```

The Ingredients API will be tied to the cocktails and users will be able to see all the ingredients in a cocktail including the boolean field to differentiate the alcoholic ingredients and non-alcoholic ones.

## Spirits

- Method: `POST`, `GET`, `GET`, `PUT`, `DELETE`
- Path: `/spirits`, `/spirits/{spirit_id}`

Input:

```json
{
  "type": string,
  "brand": string,
  "picture_url": string,
  "description": string
}
```

Output:

```json
{
  "id": int,
  "type": string,
  "brand": string,
  "owner": int,
  "picture_url": string,
  "description": string
}
```

Creating a new spirits saves the type, brand, picture, description. This adds a new existing spirits to the database which can be shown in users' liquor cabinet.
