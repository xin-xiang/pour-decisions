import { useEffect, useState, useCallback } from "react";
import { useParams } from "react-router-dom";

function CocktailDetail() {
  const params = useParams();
  const [cocktail, setCocktail] = useState({});
  const [ingredients, setIngredients] = useState([]);

  const getCocktailData = useCallback(async () => {
    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/cocktails/${params.cocktailId}`
    );

    if (response.ok) {
      const data = await response.json();
      setCocktail(data);
    }
  }, [params.cocktailId]);

  const getIngredientData = useCallback(async () => {
    const response = await fetch(
      `${process.env.REACT_APP_API_HOST}/cocktails/${params.cocktailId}/ingredients`
    );

    if (response.ok) {
      const data = await response.json();
      setIngredients(data);
    }
  }, [params.cocktailId]);

  function getAlcoholic(bool) {
    return bool ? "Yes" : "No";
  }

  useEffect(() => {
    getCocktailData();
    getIngredientData();
  }, [getCocktailData, getIngredientData]);

  return (
    <>
      <table className="table table-bordered table-hover mt-5">
        <thead>
          <tr>
            <th className="text-center">Cocktail</th>
            <th className="text-center">Flavor Profile</th>
            <th className="text-center">Picture</th>
            <th className="text-center">Description</th>
            <th className="text-center">Mixologist</th>
          </tr>
        </thead>
        <tbody>
          <tr key={cocktail.id}>
            <td className="text-center align-middle">
              {cocktail.cocktail_name}
            </td>
            <td className="text-center align-middle">{cocktail.name}</td>
            <td className="text-center align-middle">
              <img
                src={cocktail.picture_url}
                className="d-block w-100"
                alt=""
              />
            </td>
            <td className="text-center align-middle">{cocktail.description}</td>
            <td className="text-center align-middle">{cocktail.username}</td>
          </tr>
        </tbody>
      </table>
      <table className="table table-bordered table-hover mt-5">
        <thead>
          <tr>
            <th className="text-center">Ingredient</th>
            <th className="text-center">Ingredient Amounts</th>
            <th className="text-center">Is this ingredient alcoholic?</th>
          </tr>
        </thead>
        <tbody>
          {ingredients.map((ingredient) => {
            return (
              <tr key={ingredient.id}>
                <td className="text-center align-middle">{ingredient.name}</td>
                <td className="text-center align-middle">
                  {ingredient.quantity}
                </td>
                <td className="text-center align-middle">
                  {getAlcoholic(ingredient.is_spirit)}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default CocktailDetail;
