from pydantic import BaseModel
from typing import List, Optional, Union
from queries.pool import pool


class Error(BaseModel):
    message: str


class CocktailIn(BaseModel):
    cocktail_name: str
    flavor_profile: int
    picture_url: str
    description: str


class CocktailOut(BaseModel):
    id: int
    cocktail_name: str
    flavor_profile: Optional[int]
    picture_url: Optional[str]
    description: str
    mixologist: int


class UpdateCocktailIn(BaseModel):
    cocktail_name: str
    flavor_profile: Optional[int]
    picture_url: Optional[str]
    description: str


class JoinedCocktailOut(BaseModel):
    id: int
    cocktail_name: str
    picture_url: Optional[str]
    description: str
    username: str
    name: str


class CocktailRepository:
    def delete(self, cocktail_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM cocktails
                        WHERE id = %s
                        """,
                        [cocktail_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_one(self, cocktail_id: int) -> Optional[JoinedCocktailOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT c.id
                            , c.cocktail_name
                            , c.picture_url
                            , c.description
                            , u.username
                            , f.name
                        FROM cocktails AS c
                        FULL OUTER JOIN flavor_profile AS f
                        ON c.flavor_profile = f.id
                        RIGHT OUTER JOIN users AS u ON c.mixologist = u.id
                        WHERE c.id = %s
                        """,
                        [cocktail_id],
                    )
                    record = result.fetchone()
                    return self.record_to_cocktail_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not get that cocktail"}

    def update(
        self, cocktail_id: int, cocktail: CocktailIn
    ) -> Union[CocktailOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE cocktails
                        SET cocktail_name = %s
                            , flavor_profile = %s
                            , picture_url = %s
                            , description = %s
                        WHERE id = %s
                        RETURNING *
                        """,
                        [
                            cocktail.cocktail_name,
                            cocktail.flavor_profile,
                            cocktail.picture_url,
                            cocktail.description,
                            cocktail_id,
                        ],
                    )
                    mixologist = result.fetchone()[5]
                    return self.cocktail_in_to_out(
                        cocktail_id, cocktail, mixologist
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not update the cocktail"}

    def get_all(self) -> Union[Error, List[JoinedCocktailOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT c.id
                            , c.cocktail_name
                            , c.picture_url
                            , c.description
                            , u.username
                            , f.name
                        FROM cocktails AS c
                        LEFT OUTER JOIN flavor_profile AS f
                        ON c.flavor_profile = f.id
                        JOIN users AS u ON c.mixologist = u.id
                        """
                    )
                    result = []
                    for record in db:
                        cocktail = JoinedCocktailOut(
                            id=record[0],
                            cocktail_name=record[1],
                            picture_url=record[2],
                            description=record[3],
                            username=record[4],
                            name=record[5],
                        )
                        result.append(cocktail)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not get all cocktails"}

    def create(self, cocktail: CocktailIn, account_id: int) -> CocktailOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO cocktails (
                            cocktail_name,
                            flavor_profile,
                            picture_url,
                            description,
                            mixologist
                        )
                        VALUES
                            (%s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            cocktail.cocktail_name,
                            cocktail.flavor_profile,
                            cocktail.picture_url,
                            cocktail.description,
                            account_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    print(id)
                    return self.cocktail_in_to_out(id, cocktail, account_id)
        except Exception as e:
            print(e)
            return {"message": "Could not create cocktail"}

    def cocktail_in_to_out(
        self,
        id: int,
        cocktail: CocktailIn,
        mixologist: int,
    ):
        old_data = cocktail.dict()
        return CocktailOut(id=id, **old_data, mixologist=mixologist)

    def record_to_cocktail_out(self, record):
        return JoinedCocktailOut(
            id=record[0],
            cocktail_name=record[1],
            picture_url=record[2],
            description=record[3],
            username=record[4],
            name=record[5],
        )

    def get_user_cocktails(
        self, mixologist_id
    ) -> Union[Error, List[CocktailOut]]:
        try:
            with pool.connection() as conn, conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT c.id
                        , c.cocktail_name
                        , c.picture_url
                        , c.description
                        , c.mixologist
                    FROM cocktails AS c
                    WHERE c.mixologist = %s;
                    """,
                    [mixologist_id],
                )
                result = []
                for record in db:
                    cocktail = CocktailOut(
                        id=record[0],
                        cocktail_name=record[1],
                        picture_url=record[2],
                        description=record[3],
                        mixologist=record[4],
                    )
                    result.append(cocktail)
                return result
        except Exception as e:
            print(e)
            return {"message": "Could not get all cocktails"}
