import { useState } from "react";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useNavigate } from "react-router-dom";

const CreateAccount = () => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [birthday, setBirthday] = useState("");
  const { register } = useToken();
  const navigate = useNavigate();

  const handleRegistration = async (e) => {
    e.preventDefault();

    const birthDate = new Date(birthday);
    const currentDate = new Date();
    const age = currentDate.getFullYear() - birthDate.getFullYear();

    if (age < 21) {
      alert("You Don't appear to be 21 yet!");
      return;
    }

    const accountData = {
      username: username,
      email: email,
      password: password,
      birthday: birthday,
    };

    try {
      await register(
        accountData,
        `${process.env.REACT_APP_API_HOST}/api/accounts`
      );
      e.target.reset();
      navigate("/");
    } catch (error) {
      console.error("Registration failed:", error);
    }
  };
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Sign Up!</h1>
          <form onSubmit={(e) => handleRegistration(e)}>
            <div className="form-floating mb-3">
              <input
                required
                type="text"
                name="username"
                className="form-control"
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
              />
              <label className="form-label">Username</label>
            </div>
            <div className="form-floating mb-3">
              <input
                required
                type="text"
                name="password"
                className="form-control"
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
              />
              <label className="form-label">Password</label>
            </div>
            <div className="form-floating mb-3">
              <input
                required
                type="text"
                name="email"
                className="form-control"
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
              <label className="form-label">Email</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={(e) => {
                  setBirthday(e.target.value);
                }}
                value={birthday}
                placeholder="birthday"
                required
                type="text"
                name="birthday"
                id="birthday"
                className="form-control"
              />
              <label htmlFor="birthday">YYYY-MM-DD</label>
            </div>
            <button className="btn btn-primary">Sign Up!</button>
          </form>
        </div>
      </div>
    </div>
  );
};
export default CreateAccount;
