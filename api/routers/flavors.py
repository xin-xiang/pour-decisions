from fastapi import APIRouter, Depends, Response
from typing import Union, List, Optional
from authenticator import authenticator
from queries.accounts import AccountOut
from queries.flavors import (
    FlavorIn,
    FlavorRepository,
    FlavorOut,
    Error,
)


router = APIRouter()


@router.post("/flavors", response_model=Union[FlavorOut, Error])
def create_flavor(
    flavor: FlavorIn,
    response: Response,
    repo: FlavorRepository = Depends(),
):
    existing_flavor = repo.get_by_name(flavor.name)
    if existing_flavor:
        response.status_code = 400
        return Error(message="Flavor with the same name already exists")

    created_flavor = repo.create(flavor)

    if isinstance(created_flavor, Error):
        response.status_code = 400
    return created_flavor


@router.get("/flavors", response_model=Union[Error, List[FlavorOut]])
def get_all(
    current_account: AccountOut = Depends(
        authenticator.try_get_current_account_data
    ),
    repo: FlavorRepository = Depends(),
):
    return repo.get_all()


@router.delete("/flavors/{flavor_id}", response_model=bool)
def delete_flavor(
    flavor_id: int,
    repo: FlavorRepository = Depends(),
) -> bool:
    return repo.delete(flavor_id)


@router.get("/flavors/{flavor_id}", response_model=Optional[FlavorOut])
def get_one_flavor(
    flavor_id: int,
    repo: FlavorRepository = Depends(),
) -> FlavorOut:
    return repo.get_one(flavor_id)
