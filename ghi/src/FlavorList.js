import { useEffect, useState } from "react";

function FlavorList() {
  console.log("FlavorList component rendered");
  const [flavors, setFlavors] = useState([]);

  const getData = async () => {
    const response = await fetch(`${process.env.REACT_APP_API_HOST}/flavors`);

    if (response.ok) {
      const data = await response.json();
      setFlavors(data);
    } else {
      console.error("Failed to fetch data");
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <h1>Flavors</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Flavors</th>
          </tr>
        </thead>
        <tbody>
          {flavors.map((flavor) => (
            <tr key={flavor.id}>
              <td>{flavor.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default FlavorList;
