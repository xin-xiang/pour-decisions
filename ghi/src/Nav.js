import { NavLink } from "react-router-dom";
import useToken from "@galvanize-inc/jwtdown-for-react";
import { useEffect, useState } from "react";

const Nav = () => {
  const { logout, token } = useToken();
  const [username, setUsername] = useState("");

  useEffect(() => {
    if (token) {
      const user = JSON.parse(atob(token.split(".")[1])).account.username;
      setUsername(user);
    }
  }, [token]);

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          Pour Decisions
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/">
                Home
              </NavLink>
            </li>
            {!token && (
              <li className="nav-item">
                <NavLink className="nav-link" to="/signup">
                  Create Account
                </NavLink>
              </li>
            )}
            {token && (
              <li className="nav-item">
                <NavLink
                  className="nav-link active"
                  aria-current="page"
                  to="/cabinet"
                >
                  Liquor Cabinet
                </NavLink>
              </li>
            )}
            {token && (
              <li className="nav-item">
                <NavLink
                  className="nav-link active"
                  aria-current="page"
                  to="/addbottle"
                >
                  Add a Bottle
                </NavLink>
              </li>
            )}
            {token && (
              <li className="nav-item">
                <NavLink
                  className="nav-link active"
                  aria-current="page"
                  to="/cocktails"
                >
                  Create Cocktail
                </NavLink>
              </li>
            )}
            {token && (
              <li className="nav-item">
                <NavLink
                  className="nav-link active"
                  aria-current="page"
                  to="/flavors"
                >
                  Create Flavor
                </NavLink>
              </li>
            )}
            {token && (
              <li className="nav-item">
                <NavLink
                  className="nav-link active"
                  aria-current="page"
                  to="/flavors-list"
                >
                  Flavor List
                </NavLink>
              </li>
            )}
            {token && (
              <li className="nav-item">
                <NavLink
                  className="nav-link active"
                  aria-current="page"
                  to="/explore"
                >
                  Explore
                </NavLink>
              </li>
            )}
            {token && (
              <li className="nav-item">
                <NavLink
                  className="nav-link active"
                  aria-current="page"
                  to="/"
                  onClick={logout}
                >
                  Logout
                </NavLink>
              </li>
            )}
            {token && (
              <li className="nav-item">
                <NavLink
                  className="nav-link active"
                  aria-current="page"
                  to={`/accounts/${username}`}
                >
                  {username}
                </NavLink>
              </li>
            )}
            {!token && (
              <li className="nav-item">
                <NavLink
                  className="nav-link active"
                  aria-current="page"
                  to="/login"
                >
                  Login
                </NavLink>
              </li>
            )}
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Nav;
