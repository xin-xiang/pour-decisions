from fastapi import APIRouter, Depends, Response
from authenticator import authenticator
from queries.accounts import AccountOut
from queries.spirits import SpiritIn, SpiritOut, SpiritRepository, Error
from typing import Union, List

router = APIRouter()


@router.post("/spirits", response_model=Union[SpiritOut, Error])
def create_spirit(
    spirit: SpiritIn,
    current_account: AccountOut = Depends(
        authenticator.try_get_current_account_data
    ),
    repo: SpiritRepository = Depends(),
):
    if current_account:
        return repo.create(current_account["id"], spirit)


@router.get("/spirits", response_model=Union[List[SpiritOut], Error])
def get_spirits(
    repo: SpiritRepository = Depends(),
    current_account: AccountOut = Depends(
        authenticator.get_current_account_data
    ),
):
    return repo.get_spirits(current_account["id"])


@router.get("/spirits/{spirit_id}", response_model=SpiritOut)
def get_spirit(
    spirit_id: int,
    response: Response,
    current_account: AccountOut = Depends(
        authenticator.try_get_current_account_data
    ),
    repo: SpiritRepository = Depends(),
) -> SpiritOut:
    spirit = repo.get_spirit(spirit_id)
    if spirit is None:
        response.status_code = 404
    return spirit


@router.put("/spirits/{spirit_id}", response_model=Union[SpiritOut, Error])
def update_spirit(
    spirit_id: int,
    spirit: SpiritIn,
    current_account: AccountOut = Depends(
        authenticator.try_get_current_account_data
    ),
    repo: SpiritRepository = Depends(),
) -> Union[SpiritOut, Error]:
    print(current_account)
    return repo.update_spirit(current_account["id"], spirit_id, spirit)


@router.delete("/spirits/{spirit_id}", response_model=bool)
def delete_spirit(
    spirit_id: int,
    current_account: AccountOut = Depends(
        authenticator.try_get_current_account_data
    ),
    repo: SpiritRepository = Depends(),
) -> bool:
    return repo.delete_spirit(spirit_id)
