from fastapi.testclient import TestClient
from main import app
from queries.spirits import SpiritRepository
from authenticator import authenticator

client = TestClient(app)


def fake_account():
    return {
        "id": 1,
        "username": "testguy",
        "email": "testguy@email.com",
        "birthday": "1999-05-14",
    }


class EmptySpiritQueries:
    def get_spirits(self, id):
        return []


def test_get_all_spirits():

    app.dependency_overrides[SpiritRepository] = EmptySpiritQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_account

    response = client.get("/spirits")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []
